import java.util.Stack;
import java.util.HashSet;
/**
 *  This class is the main class of the "World of Zuul" application. 
 *  "World of Zuul" is a very simple, text based adventure game.  Users 
 *  can walk around some scenery. That's all. It should really be extended 
 *  to make it more interesting!
 * 
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 * 
 *  This main class creates and initialises all the others: it creates all
 *  rooms, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2016.02.29
 */

public class Game 
{
    private Parser parser;
    //private Room currentRoom;
    private Stack <Room> previous;
    private HashSet<Item> objects;
    private Player player;
    //count the number of moves that a player does
    private static int numberOfMoves;
    //set a limit of moves a player may do
    private static int limitOfMoves;

    /**
     * Create the game and initialise its internal map.
     */
    public Game() 
    {
        player = new Player();
        createRooms();
        parser = new Parser();
        previous = new Stack<>();
        objects = new HashSet<>();
        numberOfMoves = 0;
    }
    
    /**
     * Method main
     *
     * @param args A parameter
     */
    public static void main(String[] args)
    {
        Game game = new Game();
        game.play();
    }
    
    /**
     * Create all the rooms and link their exits together, connect items with rooms.
     */
    private void createRooms()
    {
        Room bedroom, bathroom, fitness, washingRoom, staircase, dollhouse, entrance, toilet, office, livingRoom, kitchen, diningRoom;

        // create the rooms
        bedroom = new Room("in your bedroom. Thinking about how you will get of the bed. If you jump, you won't survive.");
        bathroom = new Room("in the bathroom, your brother is putting gel in his hair. You see drops falling down. How do you get safely to the next room. Without getting hit by the gel?");
        fitness = new Room("in the fitness room of your house. Your dad is training. Take care that he won't hit you with a dumbell. There is a pencil and a dog cookie.");
        washingRoom = new Room("in the room with the wasing machine and the dryer. Your mom is doing the laundry. She accidentaly put you in a basket with the rest of the laundry. How wil you get out before you end up in the washingmachine?");
        staircase = new Room("at the top of the staircase you need to get down. Maybe you have something in you inventory?");
        dollhouse = new Room("suddenly in a house that fits you perfectly. But when you think about it, it could be the dollhouse of you sister. Quik hide, she is coming towards the dollhouse.");
        entrance = new Room("at the entrance of your house. The drink bowl for your dog looks very tempting. You have to drink before you can go on.");
        toilet = new Room("in the restroom. There is some toiletpaper on the ground. Maybe that can come in handy?");
        office = new Room("in the office from your dad. He sits in front of his desk. Throw the coin to get him distracted so you can run to the other room.");
        livingRoom = new Room("in the living room where your mom is drinking tea with her friend. She leaks a drop of tea right on your head. You can't go on like this.");
        kitchen = new Room("in the in the kitchen. But be careful.. Your dog lays in his basket and he doesn't like strange things.");
        diningRoom = new Room("in the dining room finally! You need to get on the table so you can drink from the liquid that will make you grow.");

        // initialise room exits
        bedroom.setExit("west", bathroom);
        
        bathroom.setExit("east", bedroom);
        bathroom.setExit("south", fitness);
        bathroom.setExit("west", staircase);

        fitness.setExit("north", bathroom);
        fitness.setExit("west", washingRoom);

        washingRoom.setExit("north", staircase);
        washingRoom.setExit("east", fitness);

        staircase.setExit("east", bathroom);
        staircase.setExit("south", washingRoom);
        staircase.setExit("west", dollhouse);
        
        dollhouse.setExit("north", entrance);
        //deze afsluiten
        dollhouse.setExit("east", staircase);
        
        entrance.setExit("north", toilet);
        entrance.setExit("east", livingRoom);
        entrance.setExit("south", dollhouse);
        
        toilet.setExit("east", office);
        toilet.setExit("south", entrance);
        
        office.setExit("south", livingRoom);
        office.setExit("west", toilet);
        
        livingRoom.setExit("north", office);
        //deze afsluiten
        livingRoom.setExit("east", kitchen);
        livingRoom.setExit("west", entrance);
        
        player.setCurrentRoom(bedroom);  // start game bedroom
        
        //Create items and put them in a room.
        bedroom.addNewItem ("pillow", "soft and fluffy", 160 );
        bedroom.addNewItem ("mobile", "it has the size of a sledge in your eyes", 70);
        
        bathroom.addNewItem ("pill strip", "you can use it as an umbrella to keep dry", 20);
        
        fitness.addNewItem ("pencil", "the pencil is a stick you can use to escape", 30);
        fitness.addNewItem ("dog cookie", "you found the favorite cookie of you dog", 20);
    }

    /**
     *  Main play routine.  Loops until end of play.
     */
    public void play() 
    {            
        printWelcome();

        // Enter the main command loop.  Here we repeatedly read commands and
        // execute them until the game is over.

        boolean finished = false;
        while (! finished) {
            Command command = parser.getCommand();
            finished = processCommand(command);
        }
        System.out.println("Thank you for playing.  Good bye.");
    }

    /**
     * Print out the opening message for the player.
     */
    private void printWelcome()
    {
        System.out.println();
        System.out.println("Welcome to the World of Zmall!");
        System.out.println("World of Zmall is a new, incredibly boring adventure game.");
        System.out.println("You have been hit by a flash of light and all you see is black. When your eyes are used to the normal light again, you notice someting strange.");
        System.out.println("Everything around you has grown. Or is it you that shrunk? Anyhow, you need to get to the dining table as soon as posible. At the dining table");
        System.out.println("is a vase with a liquid in it. You need to take a sip from that liquid so everything will be back to normal again. But watch out, because your");
        System.out.println("parents, brother, sister ànd dog are at home..");        
        System.out.println("You can naviagate by typing the word 'go' and one of the exit directions. Type 'help' if you need help. The maximum for your inventory is 150");
        System.out.println();
        System.out.println(player.getCurrentRoom().getLongDescription());
        System.out.println();
        System.out.println(player.getCurrentRoom().getItemsInRoom());
        
    }

    /**
     * Given a command, process (that is: execute) the command.
     * @param command The command to be processed.
     * @return true If the command ends the game, false otherwise.
     */
    private boolean processCommand(Command command) 
    {
        boolean wantToQuit = false;

        if(command.isUnknown()) {
            System.out.println("I don't know what you mean...");
            return false;
        }

        String commandWord = command.getCommandWord();
        if (commandWord.equals("help")) {
            printHelp();
        }
        else if (commandWord.equals("go")) {
            goRoom(command);
        }
        else if (commandWord.equals("look")){
            look();
        }
        else if(commandWord.equals("story")) {
            printStoryLine();
        }
        //else if (commandWord.equals("back")){
        //   back(command);
        //}
        else if (commandWord.equals("take")){
             player.pickUpItem(command);
         }
        // else if (commandWord.equals("drop")){
            // dropItem(command);
        // }
        else if (commandWord.equals("eat cookie")){
            System.out.println("You have eaten the magic cookie, youre stack has been upgraded!");
        }
        else if (commandWord.equals("quit")) {
            wantToQuit = quit(command);
        }
        // else command not recognised.
        return wantToQuit;
    }

    // implementations of user commands:

    /**
     * Print out some help information.
     * Here we print some stupid, cryptic message and a list of the 
     * command words.
     */
    private void printHelp() 
    {
        System.out.println("You are small. You are on your own. You wander");
        System.out.println("around in your house, looking for the magic liquid.");
        System.out.println();
        System.out.println("Your command words are:");
        System.out.println(parser.getCommandList());
        System.out.println();
    }

    /** 
     * Try to in to one direction. If there is an exit, enter the new
     * room, otherwise print an error message.
     */
    private void goRoom(Command command) 
    {
        if(!command.hasSecondWord()) {
            // if there is no second word, we don't know where to go...
            System.out.println("Go where?");
            return;
        }

        String direction = command.getSecondWord();

        // Try to leave current room.
        Room nextRoom = player.getCurrentRoom().getExit(direction);

        if (nextRoom == null) {
            System.out.println("There is no door!");
        }
        else {
            nextRoom = player.getCurrentRoom() ;
            System.out.println(player.getCurrentRoom().getLongDescription());
            System.out.println(player.getCurrentRoom().getItemsInRoom());
        }
    }
    
    /**
     * Method look. Get an description of your current room
     */
    private void look()
    {
        System.out.println(player.getCurrentRoom().getLongDescription());
    }
    

    /**
     * Method printStoryLine
     * Get the story line in case you forgot.
     */
    private void printStoryLine()
    {
        System.out.println("The situation is: ");
        System.out.println("You have been hit by a flash of light and all you see is black. When your eyes are used to the normal light again, you notice someting strange.");
        System.out.println("Everything around you has grown. Or is it you that shrunk? Anyhow, you need to get to the dining table as soon as posible. At the dining table");
        System.out.println("is a vase with a liquid in it. You need to take a sip from that liquid so everything will be back to normal again. But watch out, because your");
        System.out.println("parents, brother, sister ànd dog are at home.." );
        System.out.println("Good luck!");    
    }
    
    /**
     * Method countMove
     *
     */
    public void countMove(){
        // Count a move 
        numberOfMoves++;

        // Give some informations concerning the number of moves
        if (numberOfMoves > 2) {
            System.out.println("You have reached the maximum number of moves");
            System.out.println("By the way, GAME OVER ! ");
            System.out.println();
            System.out.println();

        }
    }

    
    
    // private void dropItem(Command command)
    // {
        // if(!command.hasSecondWord()) {
            // System.out.println("Take what?");
            // return;
        // }
        // String name = command.getSecondWord();
        
        // Item droppedItem = player.getInventory(name);
        // player.getCurrentRoom().addItem(name, droppedItem);
        // System.out.println("you have dropped " + name);
    // }
    /** 
     * "Quit" was entered. Check the rest of the command to see
     * whether we really quit the game.
     * @return true, if this command quits the game, false otherwise.
     */
    private boolean quit(Command command) 
    {
        if(command.hasSecondWord()) {
            System.out.println("Quit what?");
            return false;
        }
        else {
            return true;  // signal that we want to quit
        }
    }
}

