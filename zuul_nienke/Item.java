/**
 * An item can be used in any scene of the game.
 *
 * @author (Nienke)
 * @version (11-2017)
 */
public class Item
{
    // instance variables - replace the example below with your own
    private String name;
    private String description;
    private int weight;
    
    /**
     * Create a new item with description
     */
    public Item(String name, String description, int weight)
    {
        this.name = name;
        this.description = description;
        this.weight = weight;
    }
    
     /**
      * Method getName
      *
      * @return The return value
      */
     public String getName()
    {
        return this.name;
    }
    
    /**
     * Return a description of an item
     *
     * @return a description
     */
    public String getDescription()
    {
        return this.description;
    }
    
    /**
     * Return the weight of an item
     *
     * @return weight
     */
    public int getWeight()
    {
        return this.weight;
    }
    
    /**
     * Method itemInformation
     *
     * @return The return value
     */
    public String itemInformation()
    {
        String needToGetName = "Name:" + getName() + "\n" ;
        String needToGetDescription = "Description: "+ getDescription() + "\n" ;
        String needToGetWeight = "Weight: " + getWeight() + "\n" ;
        String needToGetAll = needToGetName + needToGetDescription + needToGetWeight + "\n" ;
        
        return needToGetAll;
    }
}
