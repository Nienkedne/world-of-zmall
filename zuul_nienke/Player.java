import java.util.HashMap;
import java.util.Stack;
/**
 * Write a description of class player here.
 *
 * @author (Nienke)
 * @version (11-2017)
 */
public class Player
{
    // instance variables - replace the example below with your own
    private Room currentRoom;
    private HashMap<String, Item> inventory;
    private int maxWeight;
    private int ongoingWeight;
    private Stack previousRoom;

    /**
     * Constructor for objects of class player
     */
    public Player()
    {
        this.currentRoom = null;
        inventory = new HashMap<String, Item>();
        maxWeight = 150;
        ongoingWeight = 0;
        previousRoom = new Stack();

        
    }

    /**
     * return the current room
     */
    public Room getCurrentRoom()
    {
        return currentRoom;
    }
    
    /**
     * Method getMaxWeight
     *
     * @return The return value
     */
    public int getMaxWeight()
    {
        return maxWeight;
    }
    
    public Stack getPreviousRoom()
    {
        return previousRoom;
    }
    
    /**
     * define the current room
     * 
     */
    public void setCurrentRoom(Room currentRoom)
    {
        this.currentRoom = currentRoom;
    }
    
    /**
     * Method setMaxWeight
     *
     * @param maxWeight A parameter
     */
    public void setMaxWeight(int maxWeight)
    {
        this.maxWeight = maxWeight;
    }
    
    /**
     * Method getInventory
     *
     * @return The return value
     */
    public HashMap getInventory()
    {
        return inventory;
    }
    
    /**
     * Method takeItem
     *
     * @param item A parameter
     */
    private void takeItem(Item item)
    {
        inventory.put(item.getName(), item);
    }
    
    /**
     * Method addItemToInventory
     *
     * this method adds an item to the player's inventory
     */
    // public void addItemToInventory(Item item)
    // {
        // if (item.getWeight() + ongoingWeight <= maxWeight)
        // {   
            // inventory.put(item.getName(), item);
            // ongoingWeight += item.getWeight();
            // System.out.println("you have" + item.getName() + " added to your inventory.") ;
        // }
        // else{
            // System.out.println("you are unable to add this to your inventory" );
        // }    
    // }
       
    /**
         * Method pickupItem
         *
         * this method picks up an item from the currentRoom  and places it in the inventory
         */
        public void pickUpItem(Command command)
        {
        if(!command.hasSecondWord()) {
            // if there is no second word, we don't know where to go...
            System.out.println("Pick up what?");
            return;
        }
        else{
            String itemName = command.getSecondWord();
        
            Item pickedUpItem = currentRoom.removeItem(itemName);
            if(pickedUpItem != null && pickedUpItem.getWeight() + ongoingWeight <= maxWeight) {
                  takeItem(pickedUpItem);
                  System.out.println("You've succesfully added " + itemName + " to your inventory!");
                }
            else {
                System.out.println("I'm sorry there is no item with that name..");
            }
        }
    }
}
